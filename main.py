# Code from Viktor


import cv2
from matplotlib import pyplot as plt

y=280
x=0
h=960
w=1280
image = cv2.imread("/Users/Vika/Desktop/test1.jpg",0)
img = image[y:y+h, x:x+w]
blur = cv2.GaussianBlur(img,(5,5),0)
ret1,th1 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
images = [img, 0, th1]

for i in range(1):
    plt.subplot(3,3,i*3+1),plt.imshow(images[i*3],'gray')
    plt.subplot(3,3,i*3+2),plt.hist(images[i*3].ravel(),256)
    plt.subplot(3,3,i*3+3),plt.imshow(images[i*3+2],'gray')

plt.show()

